#This dockerfile will build a new nginx image

FROM nginx:latest

# Copy the new index.html each time the content is changed
COPY index.html /usr/share/nginx/html/
COPY default.conf /etc/nginx/conf.d/


EXPOSE 80
EXPOSE 443

CMD ["nginx", "-g", "daemon off;"]

